import os
from argparse import ArgumentParser

def c(command):
  #os.system('command')
  print(command)

def read(args):
  print('read %(port)s' % args)
  c('cat %(gpio_path)s/gpio%(port)s/value' % args)

def export(args):
  print('export %(port)s' % args)
  c('echo %(port)s > %(gpio_path)s/export' % args)

def write(args):
  print('write %(port)s %(value)s' % args)
  c('echo %(value)s > %(gpio_path)s/gpio%(port)s/value' % args)

run = {
  'x': export,
  'r': read,
  'w': write
}

def main():
  ap = ArgumentParser()
  ap.add_argument('actions')
  ap.add_argument('ports')
  ap.add_argument('value', nargs='?', default='1')
  ap.add_argument('-p', dest='gpio_path', default='/sys/class/gpio')
  args = vars(ap.parse_args())
  that = args['ports']
  st, ed = map(int, ':'.join((that, that)).split(':'))
  for a in args['actions']:
    for p in range(st, ed+1):
      args['port'] = p
      run[a](args)

if __name__ == '__main__':
  main()
